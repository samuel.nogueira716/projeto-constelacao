var estrelas = []
function gerar_constelacao(){
    estrelas = []
    var num_estrelas = document.getElementById("num_estrelas").value
    for(i=0;i<num_estrelas;i++){
        var estrela = []
        for(a=0;a<num_estrelas;a++){
            estrela.push(gerar_numero(0,1))
        }
        estrela.splice(i,1,0)
        estrelas.push(estrela)
    }
}
function analise_estrelas(){
    var posicao_estrela_1 = document.getElementById("estrela_1").value
    var posicao_estrela_2 = document.getElementById("estrela_2").value
    var estrela_1 = estrelas[posicao_estrela_1]
    var estrela_2 = estrelas[posicao_estrela_2]

    if(estrela_1[posicao_estrela_2] == 1 && estrela_2[posicao_estrela_1] == 1){
        document.querySelector("div").innerHTML += "ESTRELAS CONECTADAS"
    }else{
        document.querySelector("div").innerHTML += "ESTRELAS NÂO CONECTADAS"
    }

}
function corrigir_matrizes(){
    var estrelas_conexoes = []
    for(i=0;i<estrelas.length;i++){
        var estrelas_conectadas = []
        estrelas.forEach(estrela => {
            if(estrela[i] == 1) estrelas_conectadas.push(estrelas.indexOf(estrela))
        })
        estrelas_conexoes.push(i)
        estrelas_conexoes.push(estrelas_conectadas)
    }
    for(a=0;a<estrelas_conexoes.length;a = a+2){
        var posicao_estrela = estrelas_conexoes[a]
        var estrela = estrelas[posicao_estrela]
        estrelas_conexoes[a+1].forEach(indice => {
            estrela.splice(indice, 1, 1)
        })
        estrelas.splice(posicao_estrela, 1, estrela)
    }
    console.log(estrelas)
}
function metodo_final(){
    if(estrelas.length>2) location.reload()
    gerar_constelacao()
    corrigir_matrizes()
    analise_estrelas()
    renderizar_resultados()
}
function renderizar_resultados(){
    var contador = 0
    estrelas.forEach(Element => {
        document.querySelector("div").innerHTML += "<br>"+ contador+"  =>   " + Element
        contador++ 
    })
}

function gerar_numero(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
