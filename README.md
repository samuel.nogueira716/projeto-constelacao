# Projeto constelação
    Projeto consiste em uma aplicação que elabore um algoritmo que receba a quantidade X (mínimo 4, máximo 8) de estrelas para formar uma constelação aleatória, mostrando a matriz formada. Após isso, receba o número de duas estrelas dessa nova constelação e informe se elas estão diretamente interligadas ou não.

## Tecnologias
HTML
CSS
JAVASCRIPT
